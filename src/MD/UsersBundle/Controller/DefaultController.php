<?php

namespace MD\UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MDUsersBundle:Default:index.html.twig');
    }
}
